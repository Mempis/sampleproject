import React from 'react';
import ReactDOM from 'react-dom';

// Container Components
import { App, Test } from './containers';

const rootElement = document.getElementById('root');
ReactDOM.render(
    <App />, rootElement
);