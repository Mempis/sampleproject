import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h3>
          Hello World
        </h3>
      </div>
    );
  }
}

export default App;
